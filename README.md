Bible-games
===========

Mini word games using the Bible.

Building up step-by-step, slowly, when free time is available ;).


Usage:
------
- Download Bible files @ http://www.info-bible.org/zip/bible.zip
- Extract files to ./Bible/Segond1910/
- Run crosswords.py to make it all work.

Requirements:
-------------

 - NLTK, with (french) stopwords and punkt
 - [Stanford tagger](http://nlp.stanford.edu/software/tagger.shtml) - version `3.4.1`
 
Notes on NLTK
-------------

`nltk.tag.stanford`'s `POSTagger` is for [part-of-speech tagging](http://en.wikipedia.org/wiki/Part-of-speech_tagging).
In POS, the words in a sentence are tagged according to their grammatical identity: nouns, verbs, adjectives, etc.


`nltk.tag.stanford`'s `NERTagger` is for [named-entity recognition](http://en.wikipedia.org/wiki/Named-entity_recognition). 
In NER, a whole sentence is analyzed, but the extracted tokens of interest are only the named entities, which are
classified into a number of categories, such as person, organization, location, expression of time, etc.

*Java Compatibility:* the Stanford tagger calls a Java program, so be careful to [use a version compatible with
your own version of Java](http://stackoverflow.com/questions/27216038/stanford-tagger-not-working).
I reverted back to version `3.4.1` of the Stanford tagger as Ubuntu defaults
to using Java 7 whereas  the last version of the Stanford tagger uses Java 8.

*Unicode:* the tagger defaults to using 'ascii', so for most languages you will want to add `encoding='utf-8'` to
the tagger parameters.
