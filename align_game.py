# -*- coding: utf-8 -*-
"""
Create game from a raw list of words

The game is to find the 'soluce' word vertically with one letter in each of
the input words (or a subset of it)
"""

import string
import itertools as it
from collections import Counter
from random import shuffle


from bible import HTMLBibleParser
from verse_processor import extract_word_list


def _check_not_subword_sym(word1, word2):
    return word2.find(word1) == -1 and word1.find(word2) == -1
    # return word1 not in word2 and word2 not in word1


def test_ordering(list_of_words, mystery_word):
    """
    find the first set of words such that each contain
    a letter from soluce word, in the right order.
    """
    game = dict()
    nb_words = len(list_of_words)
    pos = 0
    for n, word in enumerate(map(str.upper, list_of_words)):
        if pos == len(mystery_word):
            return game
        if mystery_word[pos] in word and _check_not_subword_sym(mystery_word, word):
            game[pos] = word
            pos += 1

        if pos + nb_words - n - 1 < len(mystery_word):
            return None

    if pos == len(mystery_word):
        return game
    return None


def test_different_sentences(word_sublist, words_with_sent):
    """
    check that the words come from different sentences
    return False as soon as two words from the same
    sentence are found.
    """
    used_sent = []
    for w in word_sublist.values():
        sent_id, _ = words_with_sent[w]
        if used_sent.count(sent_id) > 0:
            return False
        used_sent.append(sent_id)
    return True


def select_words(words_with_sent, mystery_word):
    """
    This greedy version checks all words ordering until one is found
    which correspond to the orders of the letters in soluce
    """
    
    if type(words_with_sent) is dict:
        list_of_words = list(words_with_sent.keys())
    else:
        list_of_words = words_with_sent
    shuffle(list_of_words)
    low = list(map(str.lower, list_of_words))
    if mystery_word in low:
        low.remove(mystery_word)
        
    letter_histogram = Counter(''.join(low))
    soluce_histogram = Counter(mystery_word.lower())
    
    # check feasibility
    diff = soluce_histogram - letter_histogram
    if any([i > 0 for i in diff.values()]):
        print('Missing letter')
        return None
        
    # Brute force: test all permutations
    for lw in it.permutations(list_of_words):
        game = test_ordering(list(lw), mystery_word)
        if game is not None:
            # adding the following test is too demanding for greedy version
            if test_different_sentences(game, words_with_sent):
                break
                
    if game is None:
        raise ValueError('no solution found in:', list_of_words)
        return dict()
        
    # get original case
    normal_game = dict()
    for pos, lword in game.items():
        low_index = low.index(lword)
        normal_game[pos] = list_of_words[low_index]
    return normal_game
    
def get_another_display(list_of_words, soluce):
    game = select_words(list_of_words, soluce)
    if game is None:
        raise ValueError('Cannot generate game')
    return game
        
    
def demo():
    
    bible_files = '/home/anne-marie/Data/Bible/'
    
    bible = HTMLBibleParser(bible_files)
    passage = bible.get_verse('Psaumes', '23')
    print(passage)
    
    words_with_def = extract_word_list(passage)
    
    input_wordplay = words_with_def
    soluce = 'berger'
    
    print(select_words(input_wordplay, soluce))
    print(get_another_display(input_wordplay, soluce))
    print(get_another_display(input_wordplay, soluce))
    game = get_another_display(input_wordplay, soluce)
    print(game)
    print(game.values())


if __name__ == "__main__":
    demo()