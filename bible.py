# -*- coding: utf-8 -*-
"""
Get verses from a Bible on disk
"""


import os.path as osp
import glob
from html.parser import HTMLParser
import codecs
import re
import unicodedata
from unescape import unescape as unes


def add_verse( data, chapter, verse, text):
    """
    add verse to book data
    nested dictionaries
    """
    if chapter not in data:
        data[chapter] = dict()
    if verse not in data[chapter]:
        data[chapter][verse] = text
    else:
        data[chapter][verse].append(text)
    return data
    
def add_space(matchobj):
    return u'\xa0' + matchobj.group(0)

def clean(text):
    """
    verse data is stored as a list of string. This function
    makes it a simple string.
    TODO: use @construction only
    """
    pattern = re.compile('\?|!|:|;')
    text = ''.join(text).strip()
    text = re.sub(pattern, add_space, text)
    return text

# adapted from: http://stackoverflow.com/questions/328356/extracting-text-from-html-file-using-python
class DeHTMLParser(HTMLParser):
    """
    Parses the data for a book html file
    The input data is quite ugly, with non-ending dt and dd tags.
    """
    def __init__(self):
        HTMLParser.__init__(self)
        self._data = dict()
        self.__text = []
        self._curr_tag = ''
        self._curr_verse = ''
        self._curr_chapter = ''

    def handle_data(self, data):
        text = unes(data).strip()
        if len(text) > 0:
            if self._last_tag == 'dd' or self._last_tag == 'font':
                text = re.sub('[ \t\r\n]+', ' ', text)
                self.__text.append(text + ' ')
            elif self._last_tag == 'dt':
                try:
                    self._curr_chapter, self._curr_verse = text.split('.')
                except ValueError:
                    pass

            
    def handle_starttag(self, tag, attrs):
        try :
            if tag == 'p':
                self.__text.append('\n\n')
            elif tag == 'br':
                self.__text.append('\n')
            elif tag == 'a':
                if attrs != [] and attrs[0][0]=='name':
                    self._curr_chapter, self._curr_verse = attrs[0][1].strip().split('.')
            else:
                self._last_tag = tag
                if tag == 'dt':
                    self._data = add_verse(self._data, 
                                           self._curr_chapter,
                                           self._curr_verse,
                                           self.__text)
                    self.__text = []
        except ValueError:
            pass

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self.__text.append('\n\n')


    def _get_full_chapter(self, chapter):
        if chapter in self._data:
            verses_id = [ int(verse) for verse in self._data[chapter].keys() if verse != '']
            return '\n'.join( [self._get_verses_by_index(chapter, verse_id) for verse_id in sorted(verses_id)])
        return None
        
    def _get_verses_by_index(self, chapter, verse_index):
        if chapter in self._data:
            return clean(self._data[chapter]['%d' % verse_index])
        return None
        
        
    def _get_verse_in_chapter(self, chapter, verse = None):
        if verse is None:
            return '\n'.join( map(clean, self._data[chapter].values()) )
        elif verse in self._data[chapter]:
            text = self._data[chapter][verse]
            return clean(text)
        vtoks = verse.split('-')
        if len(vtoks)==2:
            v1, v2 = vtoks
            verses_number = ['%d' % v for v in range(int(v1), int(v2)+1)]
            verses = map( clean, map(self._data[chapter].get, verses_number) )
            return '\n'.join(verses)
                
        return 'verse not found'
        
    def get_verse(self, chapter = None, verse = None):
        if chapter is None:
            # take all chapters
            chapters_id = [ int(c) for c in self._data.keys() if c!='']
            chapters_id = map( str, sorted(chapters_id))
            return '\n\n'.join( map(self._get_full_chapter, chapters_id) )
        elif chapter in self._data:
            return self._get_verse_in_chapter(chapter, verse)
        else:
            ctoks = chapter.split('-')
            if len(ctoks) == 2:
                c1, c2 = ctoks
                chapters_index = ['%d' % c for c in range(int(c1), int(c2)+1)]
                chapters = map(self._get_full_chapter, chapters_index)
                return '\n\n'.join(chapters)
        
        return 'chapter not found'
        
        
def decode_book_file(book_file):
    """
    Apply HTML cleaner to a file containing a bible book
    """
    book_parser = DeHTMLParser()
    fd = codecs.open(book_file, "rU", "iso-8859-15")
    text = fd.read()
    book_parser.feed(unes(text))
    book_parser.close()
    return book_parser


def remove_spaces_and_special_char(called_book):
    called_book = called_book.replace(' ', '')
    return ''.join(c for c in unicodedata.normalize('NFD', called_book)
              if unicodedata.category(c) != 'Mn')


class HTMLBibleParser:
    """
    decode all HTML bible file from a given source directory
    The Bible is downloaded from http://www.info-bible.org/zip/bible.zip
    """
    def __init__(self, source):
        self._source = source
        self._data = dict()
        self._book_files = dict()
        self._init_book_files()
        
    def _init_book_files(self):        
        book_files = glob.glob(self._source + '*.html')
        for book_file in book_files:
            _, book_filename = osp.split(book_file)
            toks = book_filename.split('.')
            if len(toks)!=3:
                continue
            bookname = toks[1]
            self._book_files[bookname] = book_file
            
    def init_book(self, bookname):
        if bookname not in self._book_files:
            raise ValueError('Unknown bookname', bookname)
            return False
        if bookname in self._data:
            return True
        book_file = self._book_files[bookname]
        book_parser = decode_book_file(book_file)
        self._data[bookname] = book_parser
        return True
            
    def load_all_books(self):
        book_files = glob.glob(self._source + '*.html')
        for book_file in book_files:
            _, book_filename = osp.split(book_file)
            toks = book_filename.split('.')
            if len(toks)!=3:
                continue
            bookname = toks[1]
            book_parser = decode_book_file(book_file)
            self._data[bookname] = book_parser


    def get_verse(self, book, chapter=None, verse=None):
        book = remove_spaces_and_special_char(book)
        if self.init_book(book):
            return self._data[book].get_verse(chapter, verse)
        return 'Book', book, 'not found, available books:\n', self._data.keys()
        
    def get_verse_and_reference(self, book, chapter=None, verse=None):
        if verse:
            if chapter is None:
                raise Exception("Cannot select a verse without a chapter!")
            ref = '%s %s:%s' % (book, chapter, verse)
        elif chapter:
            ref = '%s %s' % (book, chapter)
        else:
            ref = book
            
        return ref, self.get_verse(book, chapter, verse)
        
        
def demo():
    bible_files = '/home/anne-marie/Data/Bible/'

    
    bible = HTMLBibleParser(bible_files)
    
    print(bible.get_verse('Genèse', '1', '1'))
    print(bible.get_verse('Genèse', '1', '2'))
    print(bible.get_verse_and_reference('Genèse', '3', '15'))
    print(bible.get_verse('Actes', '1'))
    print(bible.get_verse('Jean', '3', '16-17'))
    print(bible.get_verse_and_reference('Jean', '3', '15'))
    print(bible.get_verse('Jonas', '1-2'))
    print(bible.get_verse('1 Rois', '2', '4'))
    print(bible.get_verse_and_reference('1 Rois', '3', '15'))

if __name__ == "__main__":
    demo()