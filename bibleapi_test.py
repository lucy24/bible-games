# -*- coding: utf-8 -*-
"""
test Bible.org API
http://labs.bible.org/api_web_service
"""

import json
import requests

from unescape import unescape

def get_formatted_ref(book, chapter = None, verse = None):
    ref = book
    if chapter is not None:
        ref += ' ' + chapter
    if verse is not None:
        ref += ':' + verse
    return ref

def get_verse(book, chapter = None, verse = None):
    url = 'http://labs.bible.org/api/?passage=%s' % get_formatted_ref(book, chapter, verse)
    formatting= 'plain'
    rtype = 'json'
    r = requests.get('&'.join((url, 'formatting=%s' % formatting, 'type=%s' % rtype )) )

    assert r.status_code == 200, "Passage not found"
    res = json.loads(r.text)
    assert res != [], "Passage not found"
    verses = [ unescape(v['text']) for v in res]
    return ' '.join(verses)
    
    
#print get_verse('Acts','8','34')
#print get_verse('Acts','1','7-8')
#print get_verse('Acts','1')
#print get_verse('3 John')
#print get_verse('Jude') #what's the english for Jude again?