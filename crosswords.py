# -*- coding: utf-8 -*-
"""
Kind of crosswords

Main program

Input:
    - A passage from the Bible (french)
    - The word you want to hide in the crosswords
    - Ask for the game or the solution (default is solution!)
    
Output:
    - A window displaying the game
    - A png image file with the same game.
"""

import tkinter as tk
from draw_game import *

from align_game import get_another_display
from bible import HTMLBibleParser
from verse_processor import extract_word_list, get_definitions


def drawWordPlay(words_with_offset, my_window, start_rect, view_text=True):
    pos_rect = start_rect
    start_x = start_rect[0]
    for offset, word in words_with_offset:
        pos_rect = skipNCases(offset, pos_rect)
        pos_rect = drawWord(word, my_window, pos_rect, view_text=view_text)
        pos_rect = rewindCase(pos_rect, start_x)
    return pos_rect


def drawDefinitions(definitions, my_window, posRect):
    for index, descr in enumerate(definitions):
        my_window.create_text(posRect[0], posRect[1], text='%d - %s' % (index + 1, descr),
                              font=FONTSTYLE, fill=BLACK)
        posRect = moveCaseVert(posRect)


def getDefinitionsWidth(max_def_len):
   return int(0.7 * max_def_len * FONTSIZE)


def drawTitle(title, my_window, position, do_center=True, window_width=None):
    if do_center:
        if window_width is None:
            window_width = position[3]
        position = window_width // 2, position[1], position[2], position[3]
    my_window.create_text(position[0], position[1], text=title, font=TITLE_FONTSTYLE)
    position = moveCaseVert(position)
    position = moveCaseVert(position)
    return position


def computeWordPlay(words, solution):
    """
    Take a list of N words and the solution word of lenght N
    Find the index of each solution letter in each word
    Compute the relative translation to apply to each word to get them aligned with the solution.
    The translation is given in the number of letters.

    :param words: A list of words
    :param solution: A string where for each letter li at position i, li is in the i-th word
    :return: a list of pairs (position, word)
    """
    words = list(words)
    if len(words) != len(solution):
        print('Warning: solution length does not match with list of words')
    trx = []
    for s, w in zip(solution.upper(), words):
        trx.append(w.upper().index(s))

    max_tr = max(trx)
    output = []
    for i, x in enumerate(trx):
        output.append((max_tr - x, words[i]))
    return output

    
def drawWordGame(input_words, soluce, definitions, title, view_soluce=False):
    """

    :param input_words: A list of N words
    :param soluce: A word with N letters
    :param definitions: A list with N definitions
    :param title: The title of the game
    :param view_soluce: if true, show the solution
    :return: a tkinter updated canvas.
    """

    cwordplay = computeWordPlay(input_words, soluce)
    
    
    WORD_MAX_SIZE_OFFSET = max([i+len(w) for i, w in cwordplay])
    DEF_MAX_SIZE = getDefinitionsWidth(max([len(sent) for sent in definitions]))
    TITLE_WIDTH = int(len(title) * FONTSIZE * 1.5)
    WINDOW_WIDTH = max((2 + WORD_MAX_SIZE_OFFSET) * CASE_WIDTH - LINEWIDTH, DEF_MAX_SIZE)
    WINDOW_WIDTH = max(WINDOW_WIDTH, TITLE_WIDTH)
    WINDOW_HEIGHT = 2*((2 + len(input_words)) * CASE_HEIGHT - LINEWIDTH) + 3*CASE_HEIGHT

    # create the screen
    root = tk.Tk()
    root.title(title)
    root.resizable(0, 0)
    canvas = tk.Canvas(root, width=WINDOW_WIDTH, height=WINDOW_HEIGHT, bd=0, highlightthickness=0)
    canvas.pack()
    root.update()

    center_x = WINDOW_WIDTH // 2
    pos_rect = (CASE_WIDTH, CASE_HEIGHT, CASE_WIDTH, CASE_HEIGHT)

    # draw title
    pos_rect = drawTitle(title, canvas, pos_rect, do_center=True, window_width=WINDOW_WIDTH)
    pos_rect = center_x - WORD_MAX_SIZE_OFFSET * CASE_WIDTH // 2, pos_rect[1], pos_rect[2], pos_rect[3]
    
    # draw grid
    drawWordPlay(cwordplay, canvas, pos_rect, view_soluce)
                
    # draw solution
    pos_rect = skipNCases(cwordplay[0][0] + input_words[0].index(soluce[0]), pos_rect)
    pos_rect = drawWord(soluce, canvas, pos_rect, VERTICAL, RED, view_soluce)
                
    # draw definitions
    pos_rect = rewindCase(rewindCase(pos_rect), center_x)
    pos_rect = WINDOW_WIDTH // 2, pos_rect[1], pos_rect[2], pos_rect[3]
    drawDefinitions(definitions, canvas, pos_rect)
    
    return canvas
  

def handmade_words_demo():
    input_wordplay = [
                 'Mages', 'Herode', 'Prophetie', 'Jerusalem', 'Enfant',
                 'Bethlehem', 'Etoile', 'Orient', 'encens'
                ]
    soluce = 'adoration'
    game = get_another_display(input_wordplay, soluce.lower())
    title = 'La naissance de Jésus'
    definitions = ['Ils sont venus l\'adorer',
                   'Il ne l\'a pas adoré',
                   'Elle a dit où Jésus allait naître',
                   'Les mages y ont cherché Jésus',
                   'Il est né !',
                   'La ville de David',
                   'Elle guidait les mages',
                   'Les mages en sont originaires',
                   'Ce que les mages ont offert à Jésus'
                   ]

    initial_definitions = {}
    for word, definition in zip(input_wordplay, definitions):
        initial_definitions[word] = definition
                   
    sorted_def = {}
    for word in game.values():
       sorted_def[word] = initial_definitions[word]
                   
    canvas = drawWordGame(list(game.values()), soluce.lower(), sorted_def.values(), title, view_soluce=False)
    
    # TODO: find how to save
    # pygame.image.save(winSurface, soluce + '.png')
    
    
    # run the game loop
    canvas.mainloop()


def demo(word_to_find, book, chapter):
    bible_files = '/home/anne-marie/Data/Bible/'
    
    bible = HTMLBibleParser(bible_files)
    ref, passage = bible.get_verse_and_reference(book, chapter)
    
    words_with_sent = extract_word_list(passage)
    
    input_wordplay = words_with_sent
    soluce = word_to_find.lower()

    game = get_another_display(input_wordplay, soluce)
    if game is None:
        print('Error, please try another target word.')
        return
    print('GAME FOUND:', game.values())
    
    definitions = get_definitions(game.values(), words_with_sent, 60)
    print('DEFINITIONS:', '\n'.join(definitions))

    canvas = drawWordGame(list(game.values()), soluce, definitions, ref, view_soluce=False)

    # TODO: find how to save
    canvas.postscript(file="word_align_{0}.ps".format('-'.join((book, chapter))))


    # run the game loop
    canvas.mainloop()

if __name__ == "__main__":
    # handmade_words_demo()
    # demo('Création', 'Genèse', '1')
    demo('Héros', 'Juges', '6')