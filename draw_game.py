"""
A utility module to help draw word games with Tkinter.
"""

# set up the colors
BLACK = "black"
WHITE = "white"
RED = "red"
GREEN = "green"
BLUE = "blue"

LINEWIDTH = 3
FONTSIZE = 24

WPADDING = FONTSIZE/3
HPADDING = WPADDING
CASE_WIDTH = FONTSIZE + WPADDING*2
CASE_HEIGHT = FONTSIZE + HPADDING*2

HORIZONTAL = 0
VERTICAL = 1

# set up fonts
FONTNAME = 'Arial'
FONTSTYLE = (FONTNAME, FONTSIZE)
TITLE_FONTSTYLE = (FONTNAME, FONTSIZE*2)



def drawRect(rect, canvas, color=BLACK, linewidth=LINEWIDTH):
    x, y, width, height = rect
    canvas.create_rectangle(x, y, x + width, y + height, outline=color, width=linewidth)
    return rect


def drawLetter(letter, my_window, caseRect=None, border_color=BLACK, view_text=True):

    if caseRect is None:
        caseRect = CASE_WIDTH, CASE_HEIGHT, CASE_WIDTH, CASE_HEIGHT

    drawRect(caseRect, my_window, color=border_color)

    if view_text:
        # draw the text onto the surfacet
        my_window.create_text(caseRect[0] + caseRect[2]/2, caseRect[1] + caseRect[3]/2, text=letter, font=FONTSTYLE)


def moveCaseHoriz(caseRect):
    return caseRect[0] + CASE_WIDTH, caseRect[1], caseRect[2], caseRect[3]


def moveCaseVert(caseRect):
    return caseRect[0], caseRect[1] + CASE_HEIGHT, caseRect[2], caseRect[3]


def skipNCases(nbCases, caseRect):
    if nbCases <= 0:
        return caseRect
    return caseRect[0] + nbCases*CASE_WIDTH, caseRect[1], caseRect[2], caseRect[3]


def drawWord(input_word, my_window, start_pos, direction=HORIZONTAL, border_color=BLACK, view_text=True):
    pos_rect = start_pos
    for l in input_word.upper():
        drawLetter(l, my_window, pos_rect, border_color, view_text)
        if direction is HORIZONTAL:
            pos_rect = moveCaseHoriz(pos_rect)
        else:
            pos_rect = moveCaseVert(pos_rect)
    return pos_rect


def rewindCase(caseRect, start_x=CASE_WIDTH):
    caseRect = moveCaseVert(caseRect)
    return start_x, caseRect[1], caseRect[2], caseRect[3]


def draw_grid(canvas, pos_x, pos_y, nb_cells_width, nb_cells_height, cell_size):

    horizontal_line_length = nb_cells_width * cell_size
    vertical_line_length = nb_cells_height * cell_size
    canvas.create_line(pos_x, pos_y, pos_x + horizontal_line_length, pos_y)
    canvas.create_line(pos_x, pos_y, pos_x, pos_y + vertical_line_length)

    y = pos_y
    for i in range(nb_cells_height):
        y += cell_size
        canvas.create_line(pos_x, y, pos_x + horizontal_line_length, y)

    x = pos_x
    for i in range(nb_cells_width):
        x += cell_size
        canvas.create_line(x, pos_y, x, pos_y + vertical_line_length)