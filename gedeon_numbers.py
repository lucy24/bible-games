import tkinter as tk
import math


def get_rectangle_size(n, min_width=2, mode='squarish', max_size=None):
    if max_size is None:
        if mode == 'squarish':
            max_size = math.floor(math.sqrt(n))
        else:
            max_size = n // 2
    else:
        if mode == 'max_width':
            return max_size, n // max_size, n % max_size
        if mode == 'max_height':
            return n // max_size, max_size, n % max_size
    for i in range(max_size, min_width - 1, -1):
        if n % i == 0:
            if (n // i) // i < 2:
                if n // i > i:
                    return n // i, i, 0
                return i, n // i, 0
    if max_size > n // max_size:
        return max_size, n // max_size, n % max_size
    return n // max_size, max_size, n % max_size


def window_size_needed(crowd_size, people_size=5, padding=5):
    w, h, r = get_rectangle_size(crowd_size)
    print('Draw crowd of ', crowd_size, 'with', w, h, r)
    pos_shift = people_size + padding
    return (w + int(r > 0)) * pos_shift + padding, h * pos_shift + padding


def draw_crowd(canvas, crowd_size, people_size=5, padding=5,
               color='white',
               max_size=None,
               mode='squarish'):
    w, h, r = get_rectangle_size(crowd_size, max_size=max_size, mode=mode)
    print('Draw crowd of ', crowd_size, 'with', w, h, r)
    pos_shift = people_size + padding
    for j in range(h):
        for i in range(w):
            rect_pos = padding + i*pos_shift, padding + j*pos_shift
            rect = rect_pos[0], rect_pos[1], rect_pos[0] + people_size, rect_pos[1] + people_size
            canvas.create_rectangle(*rect, fill=color)
    for i in range(r):
        rect_pos = padding + w*pos_shift, padding + i*pos_shift
        rect = rect_pos[0], rect_pos[1], rect_pos[0] + people_size, rect_pos[1] + people_size
        canvas.create_rectangle(*rect, fill=color)
    return w + (int(r>0)), h


def main():
    root = tk.Tk()
    root.title("Gédéon")
    root.resizable(0, 0)
    crowd_size = 320
    grid_params = dict(people_size=20, padding=4)
    w, h = window_size_needed(crowd_size, **grid_params)
    canvas = tk.Canvas(root, width=w, height=h, bd=0, highlightthickness=0)
    canvas.pack()
    root.update()
    # w, h = draw_crowd(canvas, crowd_size, color='red', **grid_params)
    w, h = draw_crowd(canvas, 320, color='white', mode='squarish', **grid_params)
    draw_crowd(canvas, 100, color='yellow', max_size=w, mode='max_width', **grid_params)
    # draw_crowd(canvas, 3, color='green', **grid_params)
    canvas.mainloop()

if __name__ == '__main__':
    main()