# -*- coding: utf-8 -*-
"""
Process text using NLTK


sources d'idées:
http://www.fabienpoulard.info/post/2008/03/05/Tokenisation-en-mots-avec-NLTK
http://stackoverflow.com/questions/9663918/how-can-i-tag-and-chunk-french-text-using-nltk-and-python

nltk data: stopwords, punkt
try:
     nltk.data.find('punkt.zip')
except LookupError:
     nltk.download('punkt')


"""

import string

from bible import HTMLBibleParser
import nltk
from nltk.tag.stanford import POSTagger
import re

reg_words = r'''(?x)
      \d+(\.\d+)?\s*%   # les pourcentages
    | 's                # l'appartenance anglaise 's
    | \w'               # les contractions d', l', j', t', s'
    '''
reg_words += u"| \w\u2019"  # apostrophe
reg_words += u"|\w+|[^\w\s]"
french_word_tokenizer = nltk.RegexpTokenizer(reg_words)

# TODO: config file... or better, auto-config (downloads etc.)
stanford_postagger_path = '/home/anne-marie/nltk_data/taggers/stanford-postagger-full-2014-08-27/stanford-postagger.jar'
french_tagger_path = '/home/anne-marie/nltk_data/taggers/stanford-postagger-full-2014-08-27/models/french.tagger'
french_sent_tokenizer = nltk.data.load('tokenizers/punkt/PY3/french.pickle')
french_postagger = POSTagger(french_tagger_path, path_to_jar=stanford_postagger_path, encoding='utf-8')

STOP_WORDS = nltk.corpus.stopwords.words('french')
MORE_STOP_WORDS = ['tous', 'tout', 'toute', 'toute', 'toutes',
                   'car', u'où', 'donc', 'ni', 'or', 'vers', 'quoi',
                   'lequel', 'laquelle', 'lesquels', 'lesquelles',
                   'auquel', 'duquel', 'desquels', 'parmi', u'auprès',
                   'parce', 'peu', 'environ', 'si', 'afin', 'comme',
                   'puis', 'puisque', 'assez', 'devant', 'lorsque', 'jusqu', 'lorsqu',
                   'cet', 'cette', 'cela', 'ceci', 'ceux', 'celles', 'celui',
                   'chez', 'ensuite', 'quand', 'aucun', 'près', 'voici'
                   'leurs', u'là', 'plusieurs', 'ils',
                   'chose', 'choses', 'après', 'selon', 'sans', 
                   'quelque', 'quelques', 
                   'faire', 'fallait', 'faut',
                   'dire', 'dirent', 'dis', 'dit',
                   'allait', 'allant', 'vint', 'vient', 'viens',
                   'fait', 'faites', 'fit', 'fis', 'firent', 'mis', 'mit', 'mirent']


def accept_word(word, minimum_word_length):
    if word.lower() in STOP_WORDS:
        return False
    if word.lower() in MORE_STOP_WORDS:
        return False
    if word in string.punctuation:
        return False
    if any([c in string.punctuation for c in word]):
        return False
    if len(word) < minimum_word_length:
        return False
    return True


def enumerate_nouns(passage, minimum_word_length=4):
    for sent_id, sent in enumerate(french_sent_tokenizer.tokenize(passage)):
        for word, tag in french_postagger.tag(french_word_tokenizer.tokenize(sent)):
            if tag in ('N', 'NC', 'NPP') and accept_word(word, minimum_word_length):
                yield word.lower(), sent_id, sent


def enumerate_words_with_sentences(passage, minimum_word_length=4):
    for sent_id, sent in enumerate(french_sent_tokenizer.tokenize(passage)):
        for word in french_word_tokenizer.tokenize(sent):
            if accept_word(word, minimum_word_length):
                yield word, sent_id, sent


def extract_word_list(passage):
    histo = dict()
    for word, sent_id, sent in enumerate_nouns(passage, minimum_word_length=5):
        histo[word] = (sent_id, sent)
    return histo


def simplify_sentence(sentence, word_to_keep, min_length):
    pattern = re.compile(',|:|;')
    sentence_parts = re.split(pattern, sentence)
    subfound = None
    for subsent_pos, subsent in enumerate(sentence_parts):
        if subsent.find(word_to_keep)>=0:
            subfound = subsent_pos, subsent
            break
    if subfound is None:
        return sentence
        
    sub_pos, sub_sent = subfound
    if len(sub_sent)>min_length:
        return sub_sent
    if sub_pos>0:
        begin_pos = sentence.find(sentence_parts[sub_pos-1])
        end_pos = sentence.find(sub_sent)
        return ' '.join((sentence[begin_pos:end_pos], sub_sent))
    elif sub_pos < (len(sentence_parts) -1):
        begin_pos = sentence.find(sub_sent)
        end_pos = sentence.find(sentence_parts[sub_pos+1])
        return ' '.join((sentence[begin_pos:end_pos], sentence_parts[sub_pos+1]))
    return sentence
        
    
def extract_definitions(passage, list_of_words, max_length):
    """
    Find a sentence that contains the word
    Definition is the sentence with word replaced by ...
    """
    definitions = []
    sent_used = []
    for word in list_of_words:
        for idx_sent, sent in enumerate(french_sent_tokenizer.tokenize(passage)):
            try:
                sent.index(word)
                subsent = sent
                if len(subsent)>max_length:
                    subsent = simplify_sentence(subsent, word, min_length = max_length/2)
                defi = subsent.replace('\n', '')
                defi = defi.replace(word, "...")
                definitions.append(defi)
                sent_used.append(idx_sent)
                break
            except ValueError:
                continue
                
    return definitions
    
def get_definitions(list_of_words, words_with_definitions, max_length):
    definitions = []
    for word in list_of_words:
        sent_id, subsent = words_with_definitions[word]
        if len(subsent)>max_length:
            subsent = simplify_sentence(subsent, word, min_length=max_length/3)
        defi = subsent.replace('\n','')
        defi = defi.replace(word, "...")
        definitions.append(defi)
    return definitions
    
def demo():
    """
    Examples
    """
    bible_files = '/home/anne-marie/Data/Bible/'
    
    bible = HTMLBibleParser(bible_files)
    passage = bible.get_verse('Psaumes', '23')
    print(passage)
    
    words_with_def = extract_word_list(passage)
    print(words_with_def.keys())
    print(', '.join(words_with_def.keys()))

#    words = ['berger', 'eaux', 'vallée']
    MAX_LEN = 50
    print(get_definitions(words_with_def.keys(), words_with_def, MAX_LEN))
    

if __name__ == "__main__":
    demo()