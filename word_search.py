"""
Word search game

A NxM grid in which there are words to find.
"""

import random
import string
import math

import tkinter as tk

from draw_game import *

from bible import HTMLBibleParser, remove_spaces_and_special_char
from verse_processor import enumerate_nouns


def extract_words(book, chapter, verse=None, minimum_word_length=5):
    bible_files = '/home/anne-marie/Data/Bible/'

    bible = HTMLBibleParser(bible_files)
    passage = bible.get_verse(book, chapter, verse)
    word_candidates = {remove_spaces_and_special_char(w).upper()
                       for w, _, _ in enumerate_nouns(passage, minimum_word_length)}
    return remove_duplicates(word_candidates)


def remove_duplicates(word_list):
    sorted_words = sorted(word_list)
    n_words = len(sorted_words)
    trimmed = [sorted_words[i] for i in range(n_words - 1) if sorted_words[i] not in sorted_words[i+1]]
    random.shuffle(trimmed)
    return trimmed


class WordSearchGrid:
    def __init__(self, grid_width, grid_height):
        self._width = grid_width
        self._height = grid_height
        #self._directions = [(0, 1), (0, 1), (-1, 0), (0, -1)]
        self._directions = [(0, 1), (0, 1), (1, 1)]
        self._grid = [['' for _ in range(grid_width)] for _ in range(grid_height)]
        self._list_of_words = []
        self._cache = {}

    def _init_empty(self):
        self._grid = [['' for _ in range(self._width)] for _ in range(self._height)]
        self._list_of_words = []

    def _cache_grid(self, index):
        self._cache[index] = (self._grid.copy(), self._list_of_words.copy())

    def _init_from_cache(self, index=0):
        if not self._cache:
            return
        self._grid = self._cache[index][0]
        self._list_of_words = self._cache[index][1]

    def __str__(self):
        grid = '\n'.join((' '.join(line) for line in self._grid))
        words = '\n'.join(self._list_of_words)
        return '\n'.join((grid, words))

    def _draw_position(self):
        x = random.randrange(0, self._width)
        y = random.randrange(0, self._height)
        direction = random.choice(self._directions)
        return x, y, direction

    @staticmethod
    def _get_word_position(word, position):
        x, y, direction = position
        position_vector = []
        for letter in word:
            position_vector.append((x, y, letter))
            x += direction[0]
            y += direction[1]
        return position_vector

    def _check_position_available(self, position_vector):
        for x, y, letter in position_vector:
            if not 0 <= x < self._width:
                return False
            if not 0 <= y < self._height:
                return False
            if self._grid[y][x] != '' and self._grid[y][x] != letter:
                return False
        return True

    def _add_word(self, position_vector, word):
        for x, y, letter in position_vector:
            self._grid[y][x] = letter
        self._list_of_words.append(word)

    def _try_word_insert(self, word):
        position = self._draw_position()
        position_vector = self._get_word_position(word, position)
        if self._check_position_available(position_vector):
            return position_vector
        return None

    def _random_fill(self):
        for pos_y in range(self._height):
            for pos_x in range(self._width):
                if self._grid[pos_y][pos_x] == '':
                    self._grid[pos_y][pos_x] = random.choice(string.ascii_uppercase)

    def _try_fill_from_input(self, nb_empty):
        sent = input('Suggestion:')
        if sent.lower() == 'no':
            print("That's OK, let's use random letters.")
            self._random_fill()
            return True
        letters = sent.replace(' ', '').upper()
        if len(letters) != nb_empty:
            v = 'add' if len(letters) < nb_empty else 'remove'
            nb = abs(len(letters) - nb_empty)
            print("Mmmh. Nope. Please {v} {nb} letters.".format(v=v, nb=nb))
            return False
        i = 0
        for pos_y in range(self._height):
            for pos_x in range(self._width):
                if self._grid[pos_y][pos_x] == '':
                    self._grid[pos_y][pos_x] = letters[i]
                    i += 1
        return True

    def _user_fill(self):
        nb_empty = self._count_empty_space()
        print("There are {} cells left. Can you suggest a phrase this exact length?".format(nb_empty))
        for i in range(10):
            if self._try_fill_from_input(nb_empty):
                break


    def _build(self, words):
        print('Before:', self._count_empty_space(), 'empty cells available')
        print(len(words), 'candidate words')
        for word in words:
            for i in range(self._width * self._height):
                position_vector = self._try_word_insert(word)
                if position_vector is not None:
                    break
            if position_vector is not None:
                self._add_word(position_vector, word)
        print(len(self._list_of_words), "words inserted in the grid.")
        nb_empty = self._count_empty_space()
        print(nb_empty, 'empty cells left.')
        fill_rate = 100 - round(nb_empty * 100 / (self._height * self._width))
        return fill_rate

    def build(self, words):
        best_grid = 0
        best_fill_rate = 0
        for i in range(50):
            self._init_empty()
            fill_rate = self._build(words)
            print("That's a {fill_rate}% fill rate. {comment}".format(fill_rate=fill_rate,
                                                                      comment="Rather good!" if fill_rate > 95 else
                                                                      "Not bad!" if fill_rate > 85 else "OK."))
            if fill_rate > best_fill_rate:
                best_grid = i
                best_fill_rate = fill_rate
                self._cache_grid(best_grid)


        self._init_from_cache(best_grid)
        self._user_fill()

    def _count_empty_space(self):
        return sum(x.count('') for x in self._grid)

    def draw_grid(self, canvas, x, y, cell_size=20):
        draw_grid(canvas, x, y, self._width, self._height, cell_size)

    def draw_grid_letters(self, canvas, x, y, cell_size=20, font_style=('Arial', 15)):
        for pos_y in range(self._height):
            for pos_x in range(self._width):
                letter_pos_x = x + pos_x * cell_size + cell_size // 2
                letter_pos_y = y + pos_y * cell_size + cell_size // 2
                canvas.create_text(letter_pos_x, letter_pos_y, text=self._grid[pos_y][pos_x], font=font_style)
        return canvas

    def draw_words(self, canvas, x, y, cell_size=20, font_style=('Arial', 10)):
        nb_words = len(self._list_of_words)
        words_per_col = int(math.ceil(nb_words / 4))
        start_pos_y = y + cell_size * self._height + 2 * cell_size
        word_pos_y = start_pos_y
        word_pos_x = x + 2 * cell_size
        for i, word in enumerate(sorted(self._list_of_words)):
            canvas.create_text(word_pos_x, word_pos_y, text=word, font=font_style)
            word_pos_y += cell_size
            if (i + 1) % words_per_col == 0:
                word_pos_x += cell_size * 3
                word_pos_y = start_pos_y


if __name__ == "__main__":
    game_width = 10
    game_height = 15
    cell_size = 40
    window_width = game_width * cell_size + 4 * cell_size
    window_height = game_height * cell_size + 10 * cell_size

    root = tk.Tk()
    root.title("Word Search")
    root.resizable(0, 0)
    canvas = tk.Canvas(root, width=window_width, height=window_height, bd=0, highlightthickness=0)
    canvas.pack()
    root.update()

    ref = ('Psaumes', '23')
    words = extract_words(*ref)

    word_grid = WordSearchGrid(game_width, game_height)
    word_grid.build(words)
    word_grid.draw_grid(canvas, 20, 20, cell_size)
    word_grid.draw_grid_letters(canvas, 20, 20, cell_size)
    word_grid.draw_words(canvas, 20, 20, cell_size)

    canvas.postscript(file="word_search_{0}.ps".format('-'.join(ref)))
    canvas.mainloop()